# What type of Feature do you propose?

<!--
Describe your idea/feature request below this comment and above the three dashes (---)
-->

---

## TODO:

<!-- Provide a task list if you can already say what's exactly necessary to do -->

- [ ]

## What are the benefits of a feature like this?

<!--
Describe in a few words/sentences what the biggest benefits of this
feature would be and why we need a feature like this.
-->

## What are the problems solved by this idea?

<!--
(If applicable) Describe what problem you are currently facing
and why/how this feature would tackle that issue.
Delete this section if not needed for your use-case.
-->

## Further notes

<!--
Some further space to mention things that you have not mentioned before.
This would also be the place to put some screenshots (if available) that
support your idea or explain it in a more descriptive way to the project
maintainers in order to make them understand your desire.
-->

---

<!-- In case you don't know the correct milestone just assign backlog -->

/milestone %Backlog
/label ~"Process::Needs-Triage" ~"Feature-Request"

<!--
In case you can provide a first estimate on how long this issue will need to be solved
you can provide an estimate by using the quick-action at the end of this comment.
When providing an estimate make sure the estimate includes a rough estimate from
start of work until the related branch can be merged.
If you're not sure how to estimate the issue then just leave this blank
/estimate
-->
