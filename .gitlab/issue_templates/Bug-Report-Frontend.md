# What kind of bug are you reporting?

Select (one or more) categories from below. To avoid confusion delete unnecessary fields!

- [ ] application setup does not work (properly)
  - [ ] code problem
  - [ ] other
- [ ] application does produce errors in runtime
- [ ] application unexpectedly errors (with no feedback)
- [ ] wrong visual representation
- [ ] wrong component behavior

## How can this behavior reproduced?

<!--
Write a short description about what you did,
what happened and try to describe the actual behavior
that you are reporting as unexpected/wrong/misbehavior
Delete all unnecessary steps from the list below.
-->

1. Go to (page) ...
1. Click on (component, button, etc.) ...
1. Do ...

## What did you expect to happen?

<!--
Describe what you expected to happen when performing the previous action.
These sentence beginnings should be sort of a template for a expected behavior formulation in order
to make bug report in the frontend area consistent.
Delete all examples that are not needed/used
-->

The Page/Component should show/behave/delegate...

When visiting < Page-XY > in mobile/tablet/desktop view there is an error with ...

## Further notes

<!--
Here you can and should (!!) add screenshots or other further information
that you did not yet provide somewhere before.
-->

---

<!-- In case you don't know the correct milestone just assign backlog -->

/milestone %Backlog

<!--
Possibly you might make use of ~"Bug::Responsiveness-Mobile" or ~"Bug::Responsiveness-Mobile" depending on the type of issue you are reporting
-->

/label ~"Bug"
