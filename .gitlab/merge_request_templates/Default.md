# What are the changes introduced by this MR?

*A minimal description of the changes that are introduced to the repository when accepting this Merge Request.*

## Why did you introduce this change?

*Short reasoning why this change was made, how the application/project will benefit from this change.*

## Self-Review

*With the submission of this MR I confirm I have self-reviewed the changes according to the following criteria.*

- [ ] Descriptive naming of functions, components and variables that conform to the given coding conventions

**Maintainer Opt-In:**
<!-- This checkbox will be checked in by a Maintainer, if necessary -->
- [ ] This MR requires further documentation

## Further Notes

*Further things to mention that are not mentioned before or (if applicable) screenshots.*

<!-- Possible assignees: @SvenPatrick -->
/assign @SvenPatrick
<!-- The same milestone as the issue that is solved with this MR. Defaults to %Backlog -->
/milestone %Backlog
