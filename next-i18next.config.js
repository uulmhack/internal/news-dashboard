// eslint-disable-next-line @typescript-eslint/no-var-requires
const { initReactI18next } = require("react-i18next")
module.exports = {
	fallbackLng: "en",
	i18n: {
		defaultLocale: "en",
		locales: ["en", "de"],
		keySeparator: "."
	},
	initImmediate: false,
	interpolation: {
		escapeValue: false
	},
	react: {
		bindI18n: "languageChanged",
		bindI18nStore: "",
		transEmptyNodeValue: "[NO_TRANSLATION]",
		transKeepBasicHtmlNodesFor: ["br", "strong", "i"],
		transSupportBasicHtmlNodes: true,
		useSuspense: true
	},
	serializeConfig: false,
	supportedLngs: ["en", "de"],
	use: [initReactI18next]
}
