import { ColorModeScript } from "@chakra-ui/react"
import NextDocument, { Html, Head, Main, NextScript } from "next/document"
import { theme } from "../theme"
// eslint-disable-next-line import/no-default-export
export default class Document extends NextDocument {
	// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
	render () {
		return (
			<Html>
				<Head>
					{/* <link rel="shortcut-icon" href="/favicon.png"/> */}
				</Head>
				<body>
					{/* Make Color mode to persists when you refresh the page. */}
					<ColorModeScript initialColorMode={ theme.config.initialColorMode } />
					<Main />
					<NextScript />
				</body>
			</Html>
		)
	}
}
