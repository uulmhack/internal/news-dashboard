import {
	Flex,
	Text
} from "@chakra-ui/react"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import React, { ReactElement } from "react"
import { NewsItem } from "../components/news-item"
const Index = (): ReactElement => {
	return (
		<Flex
			display="block"
			maxHeight="100%"
			maxWidth="100%"
		>
			<Text fontSize="2rem" fontWeight="bold">
				News
			</Text>
			<Text>
				Aktuelle News &amp; Tweets rund um den <code>uulmhack</code>.
				Die Anzeige aktualisiert sich laufend, um neue Inhalte zu laden.
			</Text>
			{
				[
					"gaming-pony-1",
					"gaming-cat-1",
					"gaming-fish-1",
					"gaming-cow-1",
					"gaming-octopus-1",
					"gaming-raptor-1",
					"gaming-dog-1",
					"gaming-lizzard-1",
					"gaming-snake-1",
					"gaming-bird-1",
					"gaming-bee-1",
					"gaming-dolphin-1",
					"gaming-whale-1",
					"gaming-pony-1",
					"gaming-cat-1",
					"gaming-fish-1",
					"gaming-cow-1",
					"gaming-octopus-1",
					"gaming-raptor-1",
					"gaming-dog-1",
					"gaming-lizzard-1",
					"gaming-snake-1",
					"gaming-bird-1",
					"gaming-bee-1",
					"gaming-dolphin-1",
					"gaming-whale-1",
					"gaming-pony-1",
					"gaming-cat-1",
					"gaming-fish-1",
					"gaming-cow-1",
					"gaming-octopus-1",
					"gaming-raptor-1",
					"gaming-dog-1",
					"gaming-lizzard-1",
					"gaming-snake-1",
					"gaming-bird-1",
					"gaming-bee-1",
					"gaming-dolphin-1",
					"gaming-whale-1",
					"gaming-pony-1",
					"gaming-cat-1",
					"gaming-fish-1",
					"gaming-cow-1",
					"gaming-octopus-1",
					"gaming-raptor-1",
					"gaming-dog-1",
					"gaming-lizzard-1",
					"gaming-snake-1",
					"gaming-bird-1",
					"gaming-bee-1",
					"gaming-dolphin-1",
					"gaming-whale-1",
					"gaming-pony-1",
					"gaming-cat-1",
					"gaming-fish-1",
					"gaming-cow-1",
					"gaming-octopus-1",
					"gaming-raptor-1",
					"gaming-dog-1",
					"gaming-lizzard-1",
					"gaming-snake-1",
					"gaming-bird-1",
					"gaming-bee-1",
					"gaming-dolphin-1",
					"gaming-whale-1",
					"gaming-butterfly-1"
				].map(server => {
					const randomTeaser = Math.random() * 10 > 5
						? (
							"Lorem ipsum doleret amenLorem ipsum Lorem ipsum doleret amen"
							+ "Lorem ipsum Lorem ipsum doleret amenLorem ipsum "
							+ "Lorem ipsum doleret amenLorem ipsum doleret amen "
							+ "Lorem ipsum doleret amenLorem ipsum doleret amen"
						)	: "foo bar baz exampleroni"
					const randomImage = () => Math.round((Math.random() * 10)) % 2 === 0
						? "/logo.png"
						: undefined
					return (
						<NewsItem
							postId={ server }
							key={`${server}-key`}
							shouldRenderAsHeroPost={false}
							link={"https://uulmhack.dev/news"}
							mediaUrl={ randomImage() }
							publicationDate={ new Date() }
							teaser={ randomTeaser }
							title={ server }
						/>
					)
				})
			}
		</Flex>
	)
}
export const getStaticProps = async ({ locale }) => ({
	props: {
		...await serverSideTranslations(locale, ["common"])
	}
})
// eslint-disable-next-line import/no-default-export
export default Index
