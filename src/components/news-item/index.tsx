import { ExternalLinkIcon } from "@chakra-ui/icons"
import { Flex } from "@chakra-ui/layout"
import { Image, Text } from "@chakra-ui/react"
import React, { FC } from "react"
import { INewsItemProps } from "./interface"
export const NewsItem: FC<INewsItemProps> = ({
	link,
	mediaUrl,
	postId,
	publicationDate,
	teaser,
	title,
	...rest
}) => {
	return (
		<Flex
			flexDirection="column"
			my="1.5rem"
			p="0.5rem 2rem"
			width="100%"
			height="fit-content"
			borderRadius="1rem"
			border="1px"
			borderColor="lightgrey"
			boxShadow="-1px 1px 5px grey"
			id={`news-item-${postId}`}
			_hover={ {
				boxShadow: "-3px 5px 8px rgba(0, 255, 170, 45%)",
				translate: "2px"
			} }
		>
			<Flex flexDirection="column">
				<Flex
					flexDir={["column", "column", "row"]}
					justifyContent="space-between"
					alignItems={["flex-start", "flex-start", "center"]}
					marginBottom="1rem"
				>
					<Text fontWeight="bold" fontSize="1.5rem">{ title }</Text>
					<Text
						fontSize="0.75rem"
						fontStyle="italic"
					>
						{ publicationDate.toLocaleString() }
					</Text>
				</Flex>
				<Flex flexDirection="row"alignItems="center">
					{
						mediaUrl && (
							<Image alt="" src={ mediaUrl } p="0.5rem" pr="1rem" />
						)
					}
					<Text as="p">{ teaser }</Text>
				</Flex>
				<Flex
					flexDirection="row-reverse"
				>
					<Text
						alignItems="center"
						as="a"
						display="flex"
						href={ link }
						rel="noreferrer"
						target="_blank"
						width="fit-content"
						_hover={{
							fontWeight: "bold"
						}}
					>
						more
						<ExternalLinkIcon
							ml="0.5rem"
							pb="1px"
						/>
					</Text>
				</Flex>
			</Flex>
		</Flex>
	)
}
