export interface INewsItemProps {
	link: string,
	mediaUrl?: string,
	publicationDate: Date,
	shouldRenderAsHeroPost: boolean,
	teaser: string,
	title: string,
	postId: string
}
export interface IRssFeedNewsEntry {
	topNews: boolean,
	uid: string,
	pubDate: string,
	title: string,
	link: string,
	teaser: string,
	mediaUrl?: string
}
export interface IRssFeedNewsObject {
	title: string,
	link: string,
	description: string,
	language: string,
	copyright: string,
	pubDate: string,
	generator: string,
	news: IRssFeedNewsEntry[]
}
