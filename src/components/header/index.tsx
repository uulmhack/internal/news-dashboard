/* eslint-disable quotes */
import { Box, Flex } from "@chakra-ui/layout"
import { Image, Text } from "@chakra-ui/react"
import React, { FC } from "react"
export const Header: FC = () => {
	return (
		<Flex
			flexDir={["column", "row"]}
			justifyContent="center"
			alignItems="center"
			background="#0fa"
			bgGradient="linear(to-r, #0f9 0%, #0fa 50%, #0fb 100%)"
			p="1rem"
		>
			<Text>
					uulmhack
			</Text>
			<Box
				boxSize={["50px", "75px", "100px"]}
				mx={["1rem", "3rem"]}
				my={[".5rem", "0"]}
			>
				<Image src="/logo.png" alt="uulmhack-logo"/>
			</Box>
			<Text>
					News
			</Text>
		</Flex>
	)
}
