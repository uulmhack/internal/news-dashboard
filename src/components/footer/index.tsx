import {
	Box,
	Center,
	Flex
} from "@chakra-ui/layout"
import { Text } from "@chakra-ui/react"
import React, { FC } from "react"
export const Footer: FC = () => {
	return (
		<Flex
			as="footer"
			flexDirection="column"
			bg="#0fa"
			w="100%"
			p={["1rem", "2rem"]}
		>
			<Center>
				<Box
					flexWrap="wrap"
					px={["0", "2.5rem", "5rem"]}
					py={["1rem", "0"]}
				>
				This Dashboard is not associated with the uulmhack or its organizors
				</Box>
			</Center>
			<Flex
				flexDirection={["column-reverse", "row"]}
				justifyContent={["center", "space-between"]}
			>
				<Box>
					<Text display="block" flexWrap="nowrap">
					&copy; Sven Patrick Meier
					</Text>
				</Box>
				<Box>testtesttesttesttest</Box>
			</Flex>
		</Flex>
	)
}
