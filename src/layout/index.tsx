import {
	Container,
	Spacer
} from "@chakra-ui/layout"
import { Flex } from "@chakra-ui/react"
import React, { FC } from "react"
import { Footer } from "../components/footer"
import { Header } from "../components/header"
export const LayoutContainer: FC = ({
	children
}) => {
	return (
		<Container
			padding="0"
			maxW="100%"
		>
			<Flex
				flexDir="column"
				minHeight="100vh"
				minW="100%"
			>
				<Header/>
				<Container
					py={["2rem", "5rem"]}
					px={["2rem", "25%", "auto"]}
					minW="100%"
				>
					{ children }
				</Container>
				<Spacer/>
				<Footer/>
			</Flex>
		</Container>
	)
}
