# uulmhack News-Dashboard

***Careful**: This is pre-release software, which is untested, very under-developed and not optimized for mobile screens/devices.*

A simple News-Dashboard to visualize news, announcements and other information from the official uulmhack-website.
Additionally the Dashboard displays Tweets containing the `#uulmhack` hashtag.
Also this Dashboard might contain other configurable contents in the future.

A React client application powered by NextJS, React, ChakraUI and TypeScript (based on the nextjs + chakra example).
